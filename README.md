# Bitbucket Pipelines Pipe: Sentry New Release

Notify Sentry of any Bitbucket Pipelines builds to automatically manage releases and quickly surface any errors associated with a given build.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:


```yaml
- pipe: sentryio/sentry-new-release:0.3.0
  variables:
    SENTRY_AUTH_TOKEN: '<string>'
    SENTRY_ORG: '<string>'
    SENTRY_PROJECT: '<string>'
    # SENTRY_URL: '<string>' # Optional
    # ENVIRONMENT: '<string>' # Optional.
    # FINALIZE: '<boolean>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

### Basic Usage

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| SENTRY_AUTH_TOKEN (*)   | The token used to create the release, must have the `project:releases`scope.  |
| SENTRY_ORG (*) | The Sentry organization associated with the release. |
| SENTRY_PROJECT (*) | The Sentry project associated with the release. |
| SENTRY_URL | The URL of the Sentry instance, only use when self hosting. |
| ENVIRONMENT | The environment of the deploy to create and associate with the release. |
| FINALIZE | Finalize the release (default: `true`). |
| DEBUG   | Turn on extra debug information (default: `false`). |

_(*) = required variable._


## Details

This pipe will create a release using the `$BITBUCKET_COMMIT` environment variable, and set commits automatically so that you can utilize the suspect commit feature in Sentry.
It will optionally associate a deploy to the specified environment with the created release.

## Prerequisites

In order for commits to be set appropriately for the release, you need to have your Bitbucket repository added in Sentry. Please visit [our docs](https://docs.sentry.io/workflow/integrations/global-integrations/#bitbucket) for more how to setup the Bitbucket integration in Sentry.
We also recommend storing your SENTRY_AUTH_TOKEN as a secured variable in your Bitbucket repository settings.

## Examples

Create and finalize a release:
```yaml
- pipe: sentryio/sentry-new-release:0.3.0
  variables:
    SENTRY_AUTH_TOKEN: $SENTRY_AUTH_TOKEN
    SENTRY_ORG: 'sentry-org'
    SENTRY_PROJECT: 'sentry-project'
```

Create and do not finalize a release and associate a deploy to `staging` environment with that release:
```yaml
- pipe: sentryio/sentry-new-release:0.3.0
  variables:
    SENTRY_AUTH_TOKEN: $SENTRY_AUTH_TOKEN
    SENTRY_ORG: 'sentry-org'
    SENTRY_PROJECT: 'sentry-project'
    ENVIRONMENT: 'staging'
    FINALIZE: 'false'
```

## Support

support@sentry.io
